// Import Modules
import {
  MtAActorSheet
} from "./actor-sheet.js";
import {
  MtAItemSheet
} from "./item-sheet.js";
import {
  ItemMtA
} from "./item.js";
import {
  ActorMtA
} from "./actor.js";
import {
  DiceRollerDialogue
} from "./dialogue-diceRoller.js"
import { registerSystemSettings } from "./settings.js";
import { preloadHandlebarsTemplates } from "./templates.js";
import * as migrations from "./migration.js";






CONFIG.MTA = {
  characterTypes: ["Mortal", "Sleepwalker", "Mage", "Proximi", "Vampire", "Changeling"],
  arcana: ["Death", "Fate", "Forces", "Life", "Matter", "Mind", "Prime", "Space", "Spirit", "Time"],
  practices: ["Compelling", "Knowing", "Unweiling", "Ruling", "Shielding", "Veiling", "Fraying", "Perfecting", "Weaving", "Patterning", "Unraveling", "Making", "Unmaking"],
  primaryFactors: ["Potency", "Duration"],
  cartridges: ["9mm", ".38 Special", ".44 Magnum", ".45 ACP", "30.06", "5.56mm", "12-gauge", "Arrow", "Bolt", "Fuel Canister"],
  impressions: ["Hostile","Average","Good","Excellent","Perfect"],
  skills: ["Athletics","Brawl","Drive","Firearms","Larceny","Stealth","Survival","Weaponry","Animal Ken","Empathy","Expression","Intimidation","Persuasion","Socialize","Streetwise","Subterfuge","Academics","Computer","Crafts","Investigation","Medicine","Occult","Politics","Science"],
  skills_physical: ["Athletics","Brawl","Drive","Firearms","Larceny","Stealth","Survival","Weaponry"],
  skills_social: ["AnimalKen","Empathy","Expression","Intimidation","Persuasion","Socialize","Streetwise","Subterfuge"],
  skills_mental: ["Academics","Computer","Crafts","Investigation","Medicine","Occult","Politics","Science"],
  spell_casting: {
    potency: {
      standard: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      advanced: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    },
    duration: {
      standard: ["1 turn", "2 turns", "3 turns", "5 turns", "10 turns", "20 turns", "30 turns", "40 turns", "50 turns", "60 turns", "70 turns"],
      advanced: ["1 scene/hour", "1 day", "1 week", "1 month", "1 year", "Indefinite"]
    },
    scale: {
      standard: ["1 Subject, Size 5, Arm's reach", "2 Subjects, Size 6, Small room", "4 Subjects, Size 7, Large room", "8 Subjects, Size 8, Single floor", "16 Subjects, Size 9, Small house"],
      advanced: ["5 Subjects, Size 5, Large house", "10 Subjects, Size 10, Small warehouse", "20 Subjects, Size 15, Supermarket", "40 Subjects, Size 20, Shopping mall", "80 Subjects, Size 25, City block", "160 Subjects, Size 30, Small neighborhood", "320 Subjects, Size 35, Small neighborhood", "640 Subjects, Size 40, Small neighborhood", "1280 Subjects, Size 45, Small neighborhood"]
    },
    casting_time: {
      standard: ["3 hours", "1 hour", "30 minutes", "10 minutes", "1 minute"],
      advanced: ["1 turn"]
    },
    range: {
      standard: ["Self/touch or Aimed"],
      advanced: ["Sensory", "Remote View"]
    },
    sleeper_witnesses: ["None", "One", "A few", "Large group", "Full crowd"],
    condition: ["No condition", "Improbable condition", "Infrequent condition", "Common condition"]
  },
  ephemeral_ranks: [{rank: 1, Ghost: "Poltergeist", Spirit: "Lesser Spirit", Angel: "Lesser angel", Goetia: "Lesser demon", trait_limit: 5, max_essence: 10, numina: "1-3"},
          {rank: 2, Ghost: "Minor ghost", Spirit: "Minor Spirit", Angel: "Angel", Goetia: "Fallen angel", trait_limit: 15, max_essence: 7, numina: "3-5"},
          {rank: 3, Ghost: "Average ghost", Spirit: "Average Spirit", Angel: "Archangel", Goetia: "Fallen archangel", trait_limit: 9, max_essence: 20, numina: "5-7"},
          {rank: 4, Ghost: "Powerful ghost", Spirit: "Powerful Spirit", Angel: "Principality", Goetia: "Minor Lord", trait_limit: 12, max_essence: 30, numina: "7-9"},
          {rank: 5, Ghost: "Geist", Spirit: "Minor god", Angel: "Power", Goetia: "Lord", trait_limit: 15, max_essence: 15, numina: "9-11"},
          {rank: 6, Ghost: "Geist", Spirit: "Lesser god", Angel: "Virtue", Goetia: "Sin", trait_limit: 15, max_essence: 56, numina: "11-13"},
          {rank: 7, Ghost: "Geist", Spirit: "Greater god", Angel: "Dominion", Goetia: "Archduke", trait_limit: 15, max_essence: 60, numina: "13-14"},
          {rank: 8, Ghost: "Geist", Spirit: "Lesser Celestine", Angel: "Throne", Goetia: "Duke", trait_limit: 15, max_essence: 70, numina: "14-16"},
          {rank: 9, Ghost: "Geist", Spirit: "Greater Celestine", Angel: "Cherubim", Goetia: "Prince", trait_limit: 15, max_essence: 80, numina: "16-20"},
          {rank: 10, Ghost: "Geist", Spirit: "Concept", Angel: "Seraphim", Goetia: "Emperor", trait_limit: 15, max_essence: 100, numina: "20+"}
        ],
  ephemeralTypes: ["Goetia", "Angel", "Ghost", "Spirit"],
  typeColors: {unknown: "DimGray",Mortal: "White", Sleepwalker: "CadetBlue", Proximi: "Aquamarine", Mage: "Aqua", Ghost: "BurlyWood", Spirit: "MediumPurple", Angel: "Gold", Goetia: "Indigo", Vampire: "Crimson", Changeling: "DarkGreen"},
  permissionIcons: {0: "", 1: "fas fa-low-vision", 2: "fas fa-eye", 3: "fas fa-eye"},
  gnosis_levels: [
    {mana_per_turn: 1, max_mana: 10},
    {mana_per_turn: 2, max_mana: 11},
    {mana_per_turn: 3, max_mana: 12},
    {mana_per_turn: 4, max_mana: 13},
    {mana_per_turn: 5, max_mana: 15},
    {mana_per_turn: 6, max_mana: 20},
    {mana_per_turn: 7, max_mana: 25},
    {mana_per_turn: 8, max_mana: 30},
    {mana_per_turn: 10, max_mana: 50},
    {mana_per_turn: 15, max_mana: 75}
  ],
  bloodPotency_levels: [
    {vitae_per_turn: 1, max_vitae: undefined},
    {vitae_per_turn: 1, max_vitae: 10},
    {vitae_per_turn: 2, max_vitae: 11},
    {vitae_per_turn: 3, max_vitae: 12},
    {vitae_per_turn: 4, max_vitae: 13},
    {vitae_per_turn: 5, max_vitae: 15},
    {vitae_per_turn: 6, max_vitae: 20},
    {vitae_per_turn: 7, max_vitae: 25},
    {vitae_per_turn: 8, max_vitae: 30},
    {vitae_per_turn: 10, max_vitae: 50},
    {vitae_per_turn: 15, max_vitae: 75}
  ],
  glamour_levels: [
    {glamour_per_turn: 1, max_glamour: 10},
    {glamour_per_turn: 2, max_glamour: 11},
    {glamour_per_turn: 3, max_glamour: 12},
    {glamour_per_turn: 4, max_glamour: 13},
    {glamour_per_turn: 5, max_glamour: 15},
    {glamour_per_turn: 6, max_glamour: 20},
    {glamour_per_turn: 7, max_glamour: 25},
    {glamour_per_turn: 8, max_glamour: 30},
    {glamour_per_turn: 10, max_glamour: 50},
    {glamour_per_turn: 15, max_glamour: 75}
  ],
  meleeTypes: ["Melee", "Unarmed", "Thrown"],
  devotion_actionTypes: ["Instant", "Contested", "Reflexive", "Contested (refl. resist.)"],
  rite_types: ["Rite", "Miracle"],
  rite_withstandTypes: ["Resisted by", "Contested by"],
  contract_majorTypes: ["Arcadian", "Court", "Goblin"],
  contract_ArcadianTypes: ["Crown", "Jewels", "Mirror", "Shield", "Steed", "Sword"],
  contract_CourtTypes: ["Spring", "Summer", "Autumn", "Winter"],
  contract_regalia: ["Common", "Royal"],
  pledge_types: ["Sealing", "Oath", "Bargain"]
}



/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function () {
  console.log(`Initializing MtA System`);
  /**
   * Set an initiative formula for the system
   * @type {String}
   */

  CONFIG.Combat.initiative.formula = "1d10 + @initiativeMod";

  CONFIG.Item.entityClass = ItemMtA;
  CONFIG.Actor.entityClass = ActorMtA;
  
  // Register System Settings
  registerSystemSettings();

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("dnd5e", MtAActorSheet, {
    makeDefault: true
  });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("dnd5e", MtAItemSheet, {
    makeDefault: true
  });
  // Preload Handlebars Templates
  preloadHandlebarsTemplates();
});

/**
 * Once the entire VTT framework is initialized, check to see if we should perform a data migration.
 * Small version changes (after the last dot) do not need a migration.
 */
Hooks.once("ready", function() {

  // Determine whether a system migration is required and feasible
  const currentVersion = game.settings.get("mta", "systemMigrationVersion");
  const migrationVersion = game.data.system.data.version;
  console.log("MTA Current Version: " + currentVersion);
  let version_nums_current = currentVersion.split(".");
  let version_nums_migration = migrationVersion.split(".");
  //const NEEDS_MIGRATION_VERSION = 0.84;
  //const COMPATIBLE_MIGRATION_VERSION = 0.80;
  let needMigration = (version_nums_current[0] < version_nums_migration[0]) || (version_nums_current[1] < version_nums_migration[1]) || (currentVersion === null);

  // Perform the migration
  if ( needMigration && game.user.isGM ) {
    migrations.migrateWorld();
  }
game.settings.set("mta", "systemMigrationVersion", game.data.system.data.version);
});



Hooks.on("renderChatMessage", (message, html, data) => {
  
  // Optionally collapse the description
  if (game.settings.get("mta", "autoCollapseItemDescription")) html.find(".card-description").hide();
  
  const chatCard = html.find(".chat-card");
  if (chatCard.length === 0) {
    return;
  }
  
  // If the user is the message author or the actor owner, proceed
  let actor = game.actors.get(data.message.speaker.actor);
  if (actor && actor.owner) return;
  else if (game.user.isGM || (data.author.id === game.user.id)) return;

  // Otherwise hide action buttons
  const buttons = chatCard.find("button[data-action]");
  buttons.each((i, btn) => {
    btn.style.display = "none"
  });
});

Hooks.on("renderChatLog", (app, html, data) => ItemMtA.chatListeners(html));

//Dice Roller
$(document).ready(() => {
  const diceIconSelector = '#chat-controls .chat-control-icon .fa-dice-d20';

  $(document).on('click', diceIconSelector, ev => {
    ev.preventDefault();
    let diceRoller = new DiceRollerDialogue({});
    diceRoller.render(true);
  });
});

Hooks.on("renderActorDirectory", (app, html, data) => {
  const actorListItems = html.find('li');
  
  actorListItems.toArray().forEach(v => {
    if(v.dataset.entityId){
      const actor = game.actors.get(v.dataset.entityId);
      if(actor){
        //Adds colored border to characters based on their type
        const characterType = actor.data.type === "character" ? actor.data.data.characterType : actor.data.data.ephemeralType;
        const color = game.user.isGM || actor.data.data.isTypeKnown ? CONFIG.MTA.typeColors[characterType] : CONFIG.MTA.typeColors["unknown"];
        $(v).find('img').css("border", "1px solid " + color);
        //$(v).find('a').css("color", color);

      }
      else console.log("ERROR: invalid actor found.")
    }
  });
});