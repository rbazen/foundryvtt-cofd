export class SkillEditDialogue extends Application {
  constructor(actor, skill_key, skill_type,...args) {
    super(...args);
    this.actor = actor;
    this.skill_key = skill_key;
    this.skill_type = skill_type;
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["worldbuilding", "dialogue", "mta-sheet"],
  	  template: "systems/mta/templates/dialogues/dialogue-skillEdit.html",
      resizable: true,
      width: 240,
      height: 160,
      title: "Specialties"
    });
  }
  
  getData() {
    const data = super.getData();
    data.specialties = this.actor.data.data.skills[this.skill_type][this.skill_key].specialties;
    data.isAssetSkill = this.actor.data.data.skills[this.skill_type][this.skill_key].isAssetSkill;
    
    return data;
  }
  

  activateListeners(html) {
    super.activateListeners(html);
    
    
    html.find('.add-specialty').click(ev => this._onAddSpecialty(ev));
    html.find('.remove-specialty').click(ev => this._onRemoveSpecialty(ev));
    html.find('input').change(ev => this._onChangeSpecialty(ev));
    html.find('.toggle-asset').click(ev => this._onToggleAsset(ev));
  }

  async _onAddSpecialty(event) {
    let skills = duplicate(this.actor.data.data.skills);

    if(!skills[this.skill_type][this.skill_key].specialties) skills[this.skill_type][this.skill_key].specialties = [];
    skills[this.skill_type][this.skill_key].specialties.push("New Specialty");

    let obj = {}
    obj['data.skills'] = skills;
    await this.actor.update(obj)
    this.render();
  }
  
  async _onRemoveSpecialty(event) {
    let skills = duplicate(this.actor.data.data.skills);
    const specialty_index = event.currentTarget.dataset.index;

    if(!skills[this.skill_type][this.skill_key].specialties) skills[this.skill_type][this.skill_key].specialties = [];
    skills[this.skill_type][this.skill_key].specialties.splice(specialty_index, 1);

    let obj = {}
    obj['data.skills'] = skills;
    await this.actor.update(obj)
    this.render();
  }
  
  async _onChangeSpecialty(event) {
    const specialty_index = event.currentTarget.dataset.index;
    const specialty_value = event.currentTarget.value;
    let skills = duplicate(this.actor.data.data.skills);
    skills[this.skill_type][this.skill_key].specialties[specialty_index] = specialty_value;
    
    let obj = {}
    obj['data.skills'] = skills;
    await this.actor.update(obj)
    this.render();
  }
  
   async _onToggleAsset(event) {
    let skills = duplicate(this.actor.data.data.skills);
    skills[this.skill_type][this.skill_key].isAssetSkill = !skills[this.skill_type][this.skill_key].isAssetSkill;
    
    let obj = {}
    obj['data.skills'] = skills;
    await this.actor.update(obj)
    this.render();
  }

}