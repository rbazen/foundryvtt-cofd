import {
  DiceRollerDialogue
} from "./dialogue-diceRoller.js";
/**
 * Override and extend the basic :class:`Actor` implementation
 */
export class ActorMtA extends Actor {

  /* -------------------------------------------- */
  /*	Data Preparation														*/
  /* -------------------------------------------- */

  /**
   * Augment the basic Actor data model with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

    // Get the Actor's data object
    const actorData = this.data;
    const data = actorData.data;
    
    if(!data.sizeMod) data.sizeMod = 0;
    if(!data.speedMod) data.speedMod = 0;
    if(!data.defenseMod) data.defenseMod = 0;
    if(!data.armorMod) data.armorMod = 0;
    if(!data.ballisticMod) data.ballisticMod = 0;
    if(!data.initiativeModMod) data.initiativeModMod = 0;

    // Determine Derived Traits
    if(actorData.type === "character"){
      
      let item_mods = actorData.items.reduce((acc, item) => { 
        if(item.data.equipped) {
          if(item.data.initiativeMod) acc.initiative += item.data.initiativeMod;
          if(item.type === "armor") acc.armor += item.data.rating;
          if(item.type === "armor") acc.ballistic += item.data.ballistic;
          if(item.data.defensePenalty) acc.defense -= item.data.defensePenalty;
          if(item.data.speedPenalty) acc.speed -= item.data.speedPenalty;
        }
        return acc;
      }, {initiative: 0, defense: 0, speed: 0, armor: 0, ballistic: 0});
      
      
      data.size = 5 + data.sizeMod;
      data.speed = 5 + data.attributes.physical.strength.value + data.attributes.physical.dexterity.value + data.speedMod + item_mods.speed; 
      data.defense = Math.min(data.attributes.mental.wits.value, data.attributes.physical.dexterity.value) + this._getDefenseSkill() + data.defenseMod + item_mods.defense;
      if(data.characterType === "Vampire") data.defense += data.disciplines.common.celerity.value;
      data.armor = data.armorMod + item_mods.armor;
      data.ballistic = data.ballisticMod + item_mods.ballistic;
      data.initiativeMod = data.attributes.social.composure.value + data.attributes.physical.dexterity.value + data.initiativeModMod + item_mods.initiative;
    }
    else if(actorData.type === "ephemeral"){
      data.size = 5 + data.sizeMod;
      data.speed = 5 + data.power.value + data.finesse.value + data.speedMod; 
      data.defense = Math.min(data.power.value, data.finesse.value) + data.defenseMod;
      data.armor = data.armorMod;
      data.ballistic = data.ballisticMod;
      data.initiativeMod = data.finesse.value + data.power.value + data.initiativeModMod;
    }
  }
  
  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers
  /* -------------------------------------------- */

  /** @override */
  static async create(data, options={}) {
    data.token = data.token || {};
    if ( data.type === "character" ) {
      mergeObject(data.token, {
        vision: false,
        dimSight: 30,
        brightSight: 0,
        actorLink: false,
        disposition: 0,
        displayBars: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,
        bar1: {attribute: "health"},
        displayName: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER 
      }, {overwrite: false});
    }
    return super.create(data, options);
  }
  
  //Search for Merit Defensive Combat
  _getDefenseSkill() {
    const actorData = this.data;
    const data = actorData.data;
    
    const hasBrawlMerit = this.data.items.find(ele => {
      return ele.name === "Defensive Combat (Brawl)" && ele.type === "merit";
    });
    let hasWeaponryMerit = this.data.items.find(ele => {
      return ele.name === "Defensive Combat (Weaponry)" && ele.type === "merit";
    });
    if(hasWeaponryMerit){
      hasWeaponryMerit = this.data.items.find(ele => {
        return ele.data.equipped && ele.type === "melee";
      });
    }

    const brawlSkill = hasBrawlMerit ? data.skills.physical.brawl.value : 0;
    const weaponrySkill = hasWeaponryMerit ? data.skills.physical.weaponry.value : 0;
    return Math.max(Math.max(brawlSkill, weaponrySkill), data.skills.physical.athletics.value);  
  }
  
  /**
   * Executes a perception roll using Composure + Wits.
   * if hidden is set, the roll is executed as a blind GM roll.
   */
  rollPerception(quickRoll, hidden){
    const data = this.data.data;
    let dicepool = data.attributes.social.composure.value + data.attributes.mental.wits.value;
    let flavor = "Perception: Composure + Wits";
    if(quickRoll) DiceRollerDialogue.rollToChat({dicePool: dicepool, flavor: flavor, blindGMRoll: hidden});
    else {
      let diceRoller = new DiceRollerDialogue({dicePool: dicepool, flavor: flavor, addBonusFlavor: true, blindGMRoll: true});
      diceRoller.render(true);
    }
  }
  
  /**
   * Prompts the user with a dialogue to enter name and beats to add
   * a progress entry to the actor.
   */
  addProgressDialogue(){
    let d = new Dialog({
     title: "Add Progress",
     content: "<div> <span> Name </span> <input class='attribute-value' type='text' name='input.name' placeholder='No Reason'/></div> <div> <span> Beats </span> <input class='attribute-value' type='number' name='input.beats' data-dtype='Number' min='0' placeholder='0'/></div> <div> <span> Arc. Beats </span> <input class='attribute-value' type='number' name='input.arcaneBeats' data-dtype='Number' min='0' placeholder='0'/></div>",
     buttons: {
      ok: {
       icon: '<i class="fas fa-check"></i>',
       label: "OK",
       callback: html => {
         let name = html.find(".attribute-value[name='input.name']").val();
         if(!name) name = "No Reason";
         let beats = html.find(".attribute-value[name='input.beats']").val();
         if(!beats) beats = 0;
         let arcaneBeats = html.find(".attribute-value[name='input.arcaneBeats']").val();
         if(!arcaneBeats) arcaneBeats = 0;
         this.addProgress(name, beats, arcaneBeats);
       }
      },
      cancel: {
       icon: '<i class="fas fa-times"></i>',
       label: "Cancel"
      }
     },
     default: "cancel"
    });
    d.render(true);
  }
  
  /**
   * Adds a progress entry to the actor, with given name and beats.
   */
  addProgress(name="", beats=0, arcaneBeats=0){
    const data = this.data.data;
    beats = Math.floor(beats);
    arcaneBeats = Math.floor(arcaneBeats);
    let progress = data.progress ? duplicate(data.progress) : [];
    progress.push({name: name, beats: beats, arcaneBeats: arcaneBeats});
    return this.update({'data.progress': progress});
  }
  
  /**
   * Removes a progress entry from the actor at a given position.
   * Note, that the first entry (__INITIAL__) is not part of the progress array;
   * the element coming after it has index 0.
   */
  removeProgress(index=0){
    const data = this.data.data;
    let progress = data.progress ? duplicate(data.progress) : [];
    progress.splice(index, 1);
    return this.update({'data.progress': progress});
    console.trace(data)
  }
  
  /**
   * Calculates and sets the maximum health for the actor using the formula
   * Stamina + Size.
   * If health is set lower than any damage, the damage is lost.
   */
  calculateAndSetMaxHealth(){
    const data = this.data.data;
    const maxHealth_old = data.health.max;
    let maxHealth = data.attributes.physical.stamina.value + data.size;
    //if(data.characterType === "Vampire") maxHealth += data.disciplines.common.resilience.value;
    
    let obj = {}
    obj['data.health.max'] = maxHealth;
    
    let diff = maxHealth-maxHealth_old;
    if(diff > 0){
      obj['data.health.lethal'] = "" + (+data.health.lethal + diff);
      obj['data.health.aggravated'] = "" + (+data.health.aggravated + diff);
      obj['data.health.value'] = "" + (+data.health.value + diff);
    }
    else{
      obj['data.health.lethal'] = "" + Math.max(0,(+data.health.lethal + diff));
      obj['data.health.aggravated'] = "" + Math.max(0,(+data.health.aggravated + diff));
      obj['data.health.value'] = "" + Math.max(0,(+data.health.value + diff));
    }
    this.update(obj);
  }
  
  /**
   * Calculates and sets the maximum splat-specific ressource for the actor.
   * Mage: Mana (determined by Gnosis)
   * Vampire: Vitae (determined by Blood Potency)
   */
  calculateAndSetMaxResource(){
    const data = this.data.data;
    
    if(data.characterType === "Mage"){
      let maxResource = CONFIG.MTA.gnosis_levels[Math.min(9,Math.max(0,data.gnosis-1))].max_mana; 
      let obj = {}
      obj['data.mana.max'] = maxResource;
      this.update(obj);
    }
    else if(data.characterType === "Vampire"){
      let maxResource = CONFIG.MTA.bloodPotency_levels[Math.min(10,Math.max(0,data.bloodPotency))].max_vitae;
      if(data.bloodPotency < 1) maxResource = data.attributes.physical.stamina.value;
      
      let obj = {}
      obj['data.vitae.max'] = maxResource;
      this.update(obj);
    }
  }
  
  /**
   * Calculates and sets the maximum clarity for the actor using the formula
   * Wits + Composure.
   * If clarity is set lower than any damage, the damage is lost.
   * Also calls updateChangelingTouchstones().
   */
  async calculateAndSetMaxClarity(){
    const data = this.data.data;
    const maxClarity_old = data.clarity.max;
    let maxClarity = data.attributes.mental.wits.value + data.attributes.social.composure.value;
    
    let obj = {}
    obj['data.clarity.max'] = maxClarity;
    
    let diff = maxClarity-maxClarity_old;
    if(diff > 0){
      obj['data.clarity.severe'] = "" + (+data.clarity.severe + diff);
      obj['data.clarity.value'] = "" + (+data.clarity.value + diff);
    }
    else{
      obj['data.clarity.severe'] = "" + Math.max(0,(+data.clarity.severe + diff));
      obj['data.clarity.value'] = "" + Math.max(0,(+data.clarity.value + diff));
    }
    await this.update(obj);
    this.updateChangelingTouchstones();
  }
  
  /**
   * Updates the number of touchstones based on the maximum clarity.
   */
  updateChangelingTouchstones(){
    const data = this.data.data;
    let touchstones = duplicate(data.touchstones_changeling);
    let touchstone_amount = Object.keys(touchstones).length;
    if(touchstone_amount < data.clarity.max){
      while(touchstone_amount < data.clarity.max){
        touchstones[touchstone_amount+1] = "";
        touchstone_amount = Object.keys(touchstones).length;
      }
    }
    else if(touchstone_amount > data.clarity.max){
      while(touchstone_amount > data.clarity.max){
        touchstones['-=' + touchstone_amount] = null;
        touchstone_amount -= 1;
      }
    }
    let obj = {};
    obj['data.touchstones_changeling'] = touchstones;
    this.update(obj);
  }
}
